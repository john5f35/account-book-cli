from pony import orm

from abcli.commands.test import setup_db, invoke_cmd


def test_add_account(tmp_path_factory):
    tmp_path = tmp_path_factory.mktemp('test_add_account')
    db, db_file = setup_db(tmp_path)

    res = invoke_cmd(db_file, ['account', 'add', 'TestAccount'])
    assert res.exit_code == 0, str(res)

    with orm.db_session:
        account = db.Account['TestAccount']
        assert account.name == 'TestAccount'

def test_list_account(tmp_path_factory):
    tmp_path = tmp_path_factory.mktemp('test_add_account')
    db, db_file = setup_db(tmp_path)

    with orm.db_session:
        db.Account(name="Expenses:test-account:1")
        db.Account(name="Expenses:test-account:2")

    res = invoke_cmd(db_file, ['account', 'list'])
    assert res.exit_code == 0, str(res)
    assert "Expenses:test-account:1" in res.output
    assert "Expenses:test-account:2" in res.output

    res = invoke_cmd(db_file, ['account', 'list', '-d', '2'])
    assert res.exit_code == 0, str(res)
    assert 'Expenses:test-account' in res.output
    assert not "Expenses:test-account:1" in res.output


def test_add_account_fail_already_exists(tmp_path_factory):
    tmp_path = tmp_path_factory.mktemp('test_add_account_fail_already_exists')
    db, db_file = setup_db(tmp_path)

    res = invoke_cmd(db_file, ['account', 'add', 'TestAccount'])
    assert res.exit_code == 0, str(res)

    res = invoke_cmd(db_file, ['account', 'add', 'TestAccount'])
    assert res.exit_code == 1, str(res)

    assert "Account 'TestAccount' already exists" in res.output