import logging

import click
from tabulate import tabulate
from pony import orm
from abcli.model import ACCOUNT_TYPES
from abcli.utils import error_exit_on_exception, AccountTree, account_name_at_depth

logger = logging.getLogger()

@click.group(__name__[__name__.rfind('.')+1:])
def cli():
    pass

@cli.command('list')
@click.option('--depth', '-d', type=click.IntRange(min=1, max=10), default=10,
              help="Aggregation level on account name")
@click.option('--tree', 'show_tree', is_flag=True, help="Display account name in tree form.")
@click.pass_obj
@error_exit_on_exception
def cmd_list(db, depth: int, show_tree: bool):
    with orm.db_session:
        acc_names = [a.name for a in db.Account.select()]

        tuples = []
        for acctype in ACCOUNT_TYPES:
            tree = AccountTree(acctype)
            for acc_name in filter(lambda name: name.startswith(acctype), acc_names):
                tree.add(account_name_at_depth(acc_name, depth), 0)
            if show_tree:
                tuples += tree.get_format_tuples(callback=lambda _: tuple())
            else:
                tuples += tree.get_tuples(callback=lambda _: tuple())

        click.echo(tabulate(tuples, tablefmt="simple", headers=("account", )))

@cli.command('add')
@click.argument('name')
@click.pass_obj
@error_exit_on_exception
def cmd_add(db, name: str):
    with orm.db_session:
        try:
            db.Account(name=name)
            orm.commit()
            click.echo(f"Account '{name}' added.")
            return 0
        except orm.TransactionIntegrityError:
            raise click.BadArgumentUsage(f"Account '{name}' already exists.")


@cli.command('delete')
@click.argument('name')
@click.pass_obj
@orm.db_session
@error_exit_on_exception
def cmd_delete(db, name: str):
    try:
        db.Account[name].delete()
        click.echo(f"Account '{name}' deleted.")
        return 0
    except orm.ObjectNotFound:
        raise click.BadArgumentUsage(f"Account '{name}' does not exist.")


@cli.command('show')
@click.argument('name')
@click.pass_obj
@orm.db_session
@error_exit_on_exception
def cmd_show(db, name: str):
    try:
        account = db.Account[name]
        click.echo(f"Account '{name}'")
        return 0
    except orm.ObjectNotFound:
        raise click.BadArgumentUsage(f"Account '{name}' does not exist.")
